var express = require('express');
var router = express.Router();
var users = require("../controllers/users");

// Users APIs
router.get('/api/v1/qwash/users', users.getAll);
router.get('/api/v1/qwash/users/:id', users.findById);
router.post('/api/v1/qwash/users', users.insertData);
router.put('/api/v1/qwash/users/:id', users.updateById);
router.delete('/api/v1/qwash/users/:id', users.deleteById);

module.exports = router;
