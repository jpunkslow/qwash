var express     =   require("express");
var app         =   express();
var bodyParser  =   require("body-parser");
var router      =   express.Router();
var logger		= 	require("morgan");

var mongoOp = require("./models/users");

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({"extended" : false}));

app.get('/', function(req, res, next){
    console.log("Hello World!");
});
app.use('/', require('./routes/index'));
 
// If no route is matched by now, it must be a 404
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

//--------Routes Method Request 

// router.route("/api/v1/users")
// 	.get(function(req,res){
// 		var response = {};
// 		mongoOp.find({},function(err,data){
// 			if(err){
// 				response = {"error": true, "message" : "error Fecthing data"};
// 			}else{
// 				response = {"error":false, "message" : data};
// 			}
// 			res.json(response);
// 		});	
// 	})
// 	.post(function(req,res){
// 		var db = new mongoOp();
// 		var response = {};
// 		//ambil email dan password dari rest request
// 		db.userEmail = req.body.email;
// 		db.userPassword = require('crypto').createHash('sha1').update(req.body.password).digest('base64');
// 		db.save(function(err){
// 			if(err){
// 				response = {"error": true, "message" : "error adding data" };

// 			}else{
// 				response = {"error":false,"message" : "Data Inserted" };
// 			}
// 			res.json(response);
// 		});
// 	});

// router.route("/api/v1/users/:id")
// 	.get(function(req,res){
// 		var response = {};
// 		mongoOp.findById(req.params.id,function(err,data){
// 			if(err){
// 				response = {"error":true, "message" : "error Fecthing Data"};
// 			}else{
// 				response = {"error":false, "message" : data};
// 			}
// 			res.json(response);
// 		});
// 	})
// 	.put(function(req,res){
// 		var response = {};
// 		mongoOp.findById(req.params.id,function(err,data){
// 			if(err){
// 				response = {"error":true,"message":"error fecthing data"}; 
// 			}else{
// 				if(req.body.email !== undefined){
// 					data.userEmail = req.body.email;
// 				}
// 				if(req.body.password !== undefined){
// 					data.userPassword = require('crypto').createHash('sha1').update(req.body.password).digest('base64');
// 				}
// 				data.save(function(err){
// 					if(err){
// 						response = { "error": true,"message" : "Error updating data" };
// 					}else{
// 						response = { "error": false,"message" : "Data is updated for "+req.params.id };	
// 					}
// 					res.json(response);
// 				});
// 			}

// 		});
// 	});

app.use('/',router);

app.listen(3000);
console.log("Listening to PORT 3000");