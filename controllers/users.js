var mongoOp = require("../models/users");

var users = {
	getAll : function(req,res){
		var response = {};
		mongoOp.find({},function(err,data){
			if(err){
				response = {"error": true, "message" : "error Fecthing data"};
			}else{
				response = {"error":false, "message" : data};
			}
			res.json(response);
		});	
	},
	insertData : function(req,res){
		var db = new mongoOp();
		var response = {};
		//ambil email dan password dari rest request
		db.userEmail = req.body.email;
		db.userPassword = require('crypto').createHash('sha1').update(req.body.password).digest('base64');
		db.save(function(err){
			if(err){
				response = {"error": true, "message" : "error adding data" };

			}else{
				response = {"error":false,"message" : "Data Inserted" };
			}
			res.json(response);
		});
	},
	findById : function(req,res){
		var response = {};
		mongoOp.findById(req.params.id,function(err,data){
			if(err){
				response = {"error":true, "message" : "error Fecthing Data"};
			}else{
				response = {"error":false, "message" : data};
			}
			res.json(response);
		});
	},
	updateById : function(req,res){
		var response = {};
		mongoOp.findById(req.params.id,function(err,data){
			if(err){
				response = {"error":true,"message":"error fecthing data"}; 
			}else{
				if(req.body.email !== undefined){
					data.userEmail = req.body.email;
				}
				if(req.body.password !== undefined){
					data.userPassword = require('crypto').createHash('sha1').update(req.body.password).digest('base64');
				}
				data.save(function(err){
					if(err){
						response = { "error": true,"message" : "Error updating data" };
					}else{
						response = { "error": false,"message" : "Data is updated for "+req.params.id };	
					}
					res.json(response);
				});
			}

		});
	},
	deleteById : function(req,res){
		 var response = {};
        // find the data
        mongoOp.findById(req.params.id,function(err,data){
            if(err) {
                response = {"error" : true,"message" : "Error fetching data"};
            } else {
                // data exists, remove it.
                mongoOp.remove({_id : req.params.id},function(err){
                    if(err) {
                        response = {"error" : true,"message" : "Error deleting data"};
                    } else {
                        response = {"error" : true,"message" : "Data associated with "+req.params.id+"is deleted"};
                    }
                    res.json(response);
                });
            }
        });
	}
};

module.exports = users;