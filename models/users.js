var mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/qwash');
//create instance of schema
var mongoSchema = mongoose.Schema;
//Create Schema
var userSchema = {
	"userEmail" : String,
	"userPassword" : String
};
//create model if not exists.
module.exports = mongoose.model("users",userSchema);